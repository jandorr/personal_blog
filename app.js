//jshint esversion:6

const express = require('express');
const bodyParser = require('body-parser');
const ejs = require('ejs');
const _ = require('lodash');
const mongoose = require('mongoose');
const homeStartingContent =
    'Lacus vel facilisis volutpat est velit egestas dui id ornare.';
const aboutContent =
    'Hac habitasse platea dictumst vestibulum rhoncus est pellentesque.';
const contactContent = 'Scelerisque eleifend donec pretium vulputate sapien.';

let port = process.env.PORT;
if (port == null || port == '') {
    port = 8000;
}

let dbUrl =
    'mongodb+srv://admin-jan:' +
    process.env.DB_PASSWORD +
    '@cluster0.js8fa.mongodb.net/dailyJournalDB?retryWrites=true&w=majority';
mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Connected to database.');

    const postSchema = new mongoose.Schema({
        postTitle: String,
        postBody: String,
        postBodyTruncated: String,
    });
    const Post = mongoose.model('Post', postSchema);

    const app = express();
    app.set('view engine', 'ejs');
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(express.static('public'));

    app.get('/', function (req, res) {
        Post.find(function (err, posts) {
            if (err) return console.error(err);
            res.render('home.ejs', {
                startingContent: homeStartingContent,
                posts: posts,
            });
        });
    });

    app.get('/post/:postID', function (req, res) {
        Post.findOne({ _id: req.params.postID }, function (err, post) {
            if (err) return console.error(err);
            if (post) res.render('post', post);
        });
    });

    app.get('/about', function (req, res) {
        res.render('about.ejs', { startingContent: aboutContent });
    });

    app.get('/contact', function (req, res) {
        res.render('contact.ejs', { startingContent: contactContent });
    });

    app.get('/compose', function (req, res) {
        res.render('compose.ejs');
    });

    app.post('/compose', function (req, res) {
        const post = new Post({
            postTitle: _.startCase(req.body.postTitle),
            postBody: req.body.postBody,
            postBodyTruncated: _.truncate(req.body.postBody, { length: 100 }),
        });

        post.save(function (err, post) {
            if (err) return console.error(err);
            console.log('Saved: ' + post);
            res.redirect('/');
        });
    });

    // Start server
    app.listen(port, function () {
        console.log('Server started on port ' + port);
    });
});
